export default {

  increment(state, idx ) {
    state.items[idx].value+=state.items[idx].number
    localStorage.setItem('counters', JSON.stringify(state.items) )
  },
  decrement(state, idx ) {
    state.items[idx].value-=state.items[idx].number
    localStorage.setItem('counters', JSON.stringify(state.items) )
  },

  showCreate(state) {
    state.showCreate = true
  },
  hiddenCreate(state) {
    state.showCreate = false
  },

  add(state, {id, name, value, number } ) {
    state.items.push({
      id,
      name,
      value: parseInt(value),
      number: parseInt(number)
    })
    localStorage.setItem('counters', JSON.stringify(state.items) )
  },

  load(state, items) {
    state.items = items
  },

  remove(state, idx) {
    state.items.splice(idx, 1)
    localStorage.setItem('counters', JSON.stringify(state.items) )
  },

  filterBy(state, data) {
    state.filter = data
    sessionStorage.setItem('filters', JSON.stringify(state.filter) )
  }

}
