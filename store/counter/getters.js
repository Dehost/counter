export default {

  sumCounters(state) {
    if( !state.items.length ) {
      return 0
    }

    return state.items.reduce( (prev, current) => {
      return prev + current.value
    }, 0);
  },

  totalCounters(state) {
    return state.items ? state.items.length : 0
  },

  filterBy({ items }, { options, filter, search }) {
    console.log(options)
    if ( !search ) {
      return items
    }

    let option = filter ? filter.field : options[0].field
    console.log('op',option)

    let reg = new RegExp( search, 'gi')
    return items.filter((item) => {
      console.log(item[option])
      return  reg.test(item[option])
    })
  }
}
